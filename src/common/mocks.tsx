import {
  EAppType,
  IApplicationProperties,
  ISettingPanelColor,
  ISettingProperties,
  ISettingWallpaper
} from "@common/types";

export const APPLICATIONS: IApplicationProperties[] = [
  {
    uid: '00bc4d6e-af32-421d-9e5d-bd0a7037d4f4',
    index: 0,
    name: 'Telegram',
    description: 'Описание приложения',
    icon: 'https://upload.wikimedia.org/wikipedia/commons/8/82/Telegram_logo.svg',
    type: EAppType.TEXT,
    content: 'Ой, а тут у нас контент',
    inMenu: true,
    navigation: {
      top: 24,
      left: 40
    },
    window: {
      width: 500,
      height: 400,
      top: 20,
      left: 20,
      prevTop: 0,
      prevLeft: 0,
      isMinimized: false,
      isFullScreen: false,
      isOpened: false,
      isCurrentActive: false,
      isMoving: false
    }
  },
  {
    uid: '00bc4d6e-af32-421d-9e5d-bd0a7037d4f41',
    index: 1,
    name: 'Flutter frame',
    description: 'Описание приложения',
    icon: 'https://raw.githubusercontent.com/dnfield/flutter_svg/7d374d7107561cbd906d7c0ca26fef02cc01e7c8/example/assets/flutter_logo.svg',
    type: EAppType.IFRAME,
    content: 'http://egorash.tk',
    inMenu: true,
    navigation: {
      top: 24,
      left: 40
    },
    window: {
      width: 500,
      height: 400,
      top: 20,
      left: 0,
      prevTop: 0,
      prevLeft: 0,
      isMinimized: false,
      isFullScreen: false,
      isOpened: false,
      isCurrentActive: false,
      isMoving: false
    }
  },
  {
    uid: '00bc4d6e-af32-421d-9e5d-bd0a7037d4f42',
    index: 2,
    name: 'Yandex Home',
    description: 'Описание приложения',
    icon: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/Alisa_Yandex.svg/240px-Alisa_Yandex.svg.png',
    type: EAppType.IFRAME,
    content: 'https://yandex.ru/quasar/iot/',
    inMenu: true,
    navigation: {
      top: 24,
      left: 40
    },
    window: {
      width: 500,
      height: 400,
      top: 20,
      left: 20,
      prevTop: 0,
      prevLeft: 0,
      isMinimized: false,
      isFullScreen: false,
      isOpened: false,
      isCurrentActive: false,
      isMoving: false
    }
  },
]

export const SETTINGS: ISettingProperties = {
  isReload: false,
  theme: {
    primaryColor: '#333333',
    textColor: 'white'
  },
  desktop: {
    background: {
      url: null,
      color: '#888'
    }
  },
  navigation: {
    menu: {
      isOpen: false
    },
    application: {
      tabSize: 36,
      iconSize: 24,
      width: 0,
      isCenter: false
    },
    panel: {
      isOpen: false
    }
  }
}

export const SETTING_PANEL_COLOR: ISettingPanelColor[] = [
  {
    name: 'dark',
    color: '#222222',
    isBlackFontColor: false
  },
  {
    name: 'white',
    color: '#ffffff',
    isBlackFontColor: true
  },
  {
    name: 'gray',
    color: '#aaaaaa',
    isBlackFontColor: true
  },
  {
    name: 'red',
    color: '#D32F2F',
    isBlackFontColor: false
  },
  {
    name: 'blue',
    color: '#3949AB',
    isBlackFontColor: false
  },
  {
    name: 'green',
    color: '#2E7D32',
    isBlackFontColor: false
  },
  {
    name: 'violet',
    color: '#512DA8',
    isBlackFontColor: false
  }
]

export const SETTING_WALLPAPER: ISettingWallpaper[] = [
  {
    url: 'https://oboi.ws/filters/toaster_17_12402_oboi_krasnyj_firewatch_1920x1080.jpg'
  },
  {
    url: 'https://oboi.ws/originals/original_489_oboi_oboi_na_rabochij_stol_windows_1920x1080.jpg'
  },
  {
    url: 'https://klike.net/uploads/posts/2019-11/1572612050_1.jpg'
  },
]