import styled from "styled-components";
import {ISDesktopProperties, ISettingWallpaper} from "@common/types";
import {DEFAULT_TRANSITION} from "@common/constants";

export const MainContainer = styled.main<ISettingWallpaper>`
  transition: ${DEFAULT_TRANSITION};
  background: url(${({ url }) => url}) no-repeat center;
  background-color: ${({ color }) => color};
  background-size: cover;
  position: relative;
  width: 100%;
  height: 100vh;
  overflow: hidden;
`;