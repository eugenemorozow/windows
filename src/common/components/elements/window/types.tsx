import {IApplicationProperties, IDefaultProps, ISettingProperties} from "@common/types";
import React from "react";

export interface IWindowProps {
  application: IApplicationProperties
  settings: ISettingProperties
}

export interface IWindowIframeProps {
  src: string
}