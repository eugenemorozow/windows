import React, {FC, memo, useEffect} from 'react';
import {IWindowIframeProps} from "@common/components/elements/window/types";

const WindowIframe: FC<IWindowIframeProps> = ({ src }) => {
  return (
    <iframe src={src}
            style={{ display: 'flex', border: 'none' }}
            width="100%"
            height="100%"
    />
  )
}

export default memo(WindowIframe)