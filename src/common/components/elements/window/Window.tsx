import React, { FC, MouseEvent, memo, useEffect, useRef, useState } from 'react';
import { IWindowProps } from "./types";
import {
  WindowContent,
  WindowHeader,
  WindowHeaderButtons,
  WindowHeaderDragger,
  WindowHeaderTitle,
  WindowWrapper
} from "./Window.styles";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowDown, faTimes } from '@fortawesome/free-solid-svg-icons'
import { faSquare } from '@fortawesome/free-regular-svg-icons'
import { useActions } from "@hooks/useActions";
import { WINDOW_HEADER_DRAGGER } from "@common/constants";
import { EAppType } from '@common/types';
import WindowIframe from './WindowIframe';

const Window: FC<IWindowProps> = ({ application, settings  }) => {
  const actions = useActions()

  const setMinimize = () => {
    actions.setApplication(application.uid, {
      windowEdit: {
        isMinimized: true,
        isCurrentActive: false
      }
    })
  }

  const setFullScreen = () => {
    actions.setApplication(application.uid, {
      windowEdit: {
        isFullScreen: !application.window.isFullScreen
      }
    })
  }

  const setCurrentActive = () => {
    if (!application.window.isMinimized && application.window.isOpened && !application.window.isCurrentActive) {
      actions.setApplication(application.uid, {
        windowEdit: {
          isCurrentActive: true
        }
      })
    }
  }

  const setClose = () => {
    const payload = {
      windowEdit: {
        isOpened: false,
        isMinimized: false,
        isCurrentActive: false
      }
    }
    actions.setApplication(application.uid, payload)
  }

  return (
    <WindowWrapper id={`window_${application.uid}`}
                   className="windowWrapper"
                   prop={application}
                   onClick={setCurrentActive}
    >
      <WindowHeader id={`window_header_${application.uid}`}
                    primaryColor={settings.theme.primaryColor}
                    textColor={settings.theme.textColor}
                    className="windowHeader"
      >
        <WindowHeaderTitle icon={application?.icon}>
          {application?.icon && <span className="icon" />}
          <span>
            {application.name}
          </span>
        </WindowHeaderTitle>
        <WindowHeaderDragger id={application.uid} className={WINDOW_HEADER_DRAGGER} onDoubleClick={setFullScreen} />
        <WindowHeaderButtons>
          <div onClick={setMinimize}>
            <FontAwesomeIcon icon={faArrowDown} />
          </div>
          <div id="maximize" onClick={setFullScreen}>
            <FontAwesomeIcon icon={faSquare} />
          </div>
          <div id="close" className="exit" onClick={setClose}>
            <FontAwesomeIcon icon={faTimes} />
          </div>
        </WindowHeaderButtons>
      </WindowHeader>

      <WindowContent>
        { (application.type == EAppType.IFRAME) ? <WindowIframe src={application.content} />: application.content }
      </WindowContent>
    </WindowWrapper>
  )
}

export default Window