import styled from "styled-components";
import {DEFAULT_TRANSITION} from "@common/constants";
import {IApplicationChangeParameters, IApplicationProperties, ISThemeEditProperties} from "@common/types";

interface IWindowStyle {
  prop: IApplicationProperties
}

export const WindowWrapper = styled.div<IWindowStyle>`
  transition: ${({ prop }) => prop.window.isMoving ? 'unset' : DEFAULT_TRANSITION };
  display: flex;
  flex-direction: column;
  max-width: 100%;
  max-height: 100%;
  min-width: 320px;
  min-height: 80px;
  resize: both;
  position: absolute;
  top: ${({ prop }) => `${prop.window.top}px`};
  left: ${({ prop }) => `${prop.window.left}px`};
  width: ${({ prop }) => prop.window.isFullScreen && !prop.window.isMinimized ? `100%` 
                              : prop.window.isMinimized || !prop.window.isOpened ? `0px` 
                                      : `${prop.window.width}px`};
  height: ${({ prop }) => prop.window.isFullScreen && !prop.window.isMinimized ? `calc(100% - 48px)`
                              : prop.window.isMinimized || !prop.window.isOpened ? `0px` 
                                      : `${prop.window.height}px`};
  box-shadow: rgba(100, 100, 111, 0.2) 0 7px 29px 0;
  background: white;
  overflow: hidden;
  z-index: ${({ prop }) => prop.window.isCurrentActive ? 9 : 8 };
`;

export const WindowHeader = styled.header<ISThemeEditProperties>`
  background: ${({ primaryColor }) => primaryColor ?? '#333'};
  color: ${({ textColor }) => textColor ?? 'white'};
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const WindowContent = styled.main`
  width: 100%;
  height: 100%;
  font-size: 14px;
  font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
  Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
`;

export const WindowHeaderTitle = styled.div<IApplicationChangeParameters>`
  pointer-events: all;
  padding: 10px;
  font-size: 14px;
  user-select: none;
  position: absolute;
  z-index: 0;
  display: flex;
  align-items: center;
  
  span {
    &.icon {
      display: block;
      width: 16px;
      height: 16px;
      margin-right: 10px;
      background: url(${({ icon }) => {
        return icon
      }}) no-repeat center transparent;
      background-size: cover;
      border-radius: 50%;
    }
  }
`;

export const WindowHeaderDragger = styled.div`
  width: 100%;
  height: 100%;
  z-index: 1;
`;

export const WindowHeaderButtons = styled.div`
  display: flex;
  font-size: 14px;
  
  div {
    transition: ${DEFAULT_TRANSITION};
    padding: 10px 20px;
    cursor: pointer;
    
    &:hover {
      background: rgba(255,255,255,.2);
    }
    
    &.exit:hover {
      background: #B71C1C;
    }
  }
`;