import React, {FC, useState} from 'react';
import {
  NavigationSettingButton,
  NavigationSettingCard,
  NavigationSettingColorItem, NavigationSettingContainer,
  NavigationSettingHeader,
  NavigationSettingPanel,
  NavigationSettingsClock,
  NavigationSettingsClockDate,
  NavigationSettingsClockTime,
  NavigationSettingsContainer, NavigationSettingWallpaperItem
} from "@common/components/elements/navigation/Navigation.styles";
import {useInterval} from "@hooks/useInterval";
import moment from 'moment';
import {INavigationClock, ISettingPanelProps} from "@common/components/elements/navigation/types";
import {useActions} from "@hooks/useActions";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPalette, faImage, faAlignCenter} from "@fortawesome/free-solid-svg-icons";
import {SETTING_PANEL_COLOR, SETTING_WALLPAPER} from "@common/mocks";

const NavigationSettings: FC<ISettingPanelProps> = ({ settings, calculateItemPosition, applications }) => {
  const actions = useActions()
  const [clock, setClock] = useState<INavigationClock>({
    date: moment(new Date()).format('DD.MM.YYYY'),
    time: moment(new Date()).format('HH:mm')
  })

  useInterval(() => {
    const date = moment(new Date()).format('DD.MM.YYYY')
    const time = moment(new Date()).format('HH:mm')
    if (date !== clock.date || time !== clock.time) {
      setClock({ date, time })
    }
  }, 1000)

  const setPanelShowing = () => {
    actions.setSetting({
      navigation: {
        panel: {
          isOpen: !settings.navigation.panel.isOpen
        }
      }
    })
  }

  const editPanelPosition = () => {
    actions.setSetting({
      navigation: {
        application: {
          width: (applications.filter(it => it.navigation).length * 48 + 10) - 10,
          isCenter: !settings.navigation.application.isCenter
        }
      }
    })
    setTimeout(calculateItemPosition, 100)
  }

  const setWallpaper = (url: string) => {
    actions.setSetting({
      desktop: {
        background: {
          url: url
        }
      }
    })
  }

  const setPrimaryColor = (color: string, isBlackFont: boolean) => {
    actions.setSetting({
      theme: {
        primaryColor: color,
        textColor: isBlackFont ? 'black' : 'white'
      }
    })
  }

  return (
    <NavigationSettingsContainer>
      {
        settings.navigation.panel.isOpen && (
          <NavigationSettingPanel id="settingPanel"
                                  primaryColor={settings.theme.primaryColor}
                                  textColor={settings.theme.textColor}>
            <NavigationSettingCard>
              <NavigationSettingHeader>
                <FontAwesomeIcon icon={faImage} /> Обои
              </NavigationSettingHeader>
              <NavigationSettingContainer>
                {
                  SETTING_WALLPAPER.map(it =>
                    <NavigationSettingWallpaperItem 
                      key={it.url}
                      property={it.url}
                      onClick={() => setWallpaper(it?.url || '')} 
                    />
                  )
                }
              </NavigationSettingContainer>
            </NavigationSettingCard>

            <NavigationSettingCard>
              <NavigationSettingHeader>
                <FontAwesomeIcon icon={faPalette} /> Цвета
              </NavigationSettingHeader>
              <NavigationSettingContainer>
                {
                  SETTING_PANEL_COLOR.map(it =>
                    <NavigationSettingColorItem key={it.color}
                                                color={it.color}
                                                onClick={() => setPrimaryColor(it.color, it.isBlackFontColor)} />)
                }
              </NavigationSettingContainer>
            </NavigationSettingCard>

            <NavigationSettingCard>
              <NavigationSettingHeader>
                <FontAwesomeIcon icon={faAlignCenter} /> Панель
              </NavigationSettingHeader>
              <NavigationSettingButton onClick={editPanelPosition}>
                { settings.navigation.application.isCenter ? 'По центру' : 'С края' }
              </NavigationSettingButton>
            </NavigationSettingCard>
          </NavigationSettingPanel>
        )
      }
      <NavigationSettingsClock onClick={setPanelShowing}>
        <NavigationSettingsClockTime>{ clock.time }</NavigationSettingsClockTime>
        <NavigationSettingsClockDate>{ clock.date }</NavigationSettingsClockDate>
      </NavigationSettingsClock>
    </NavigationSettingsContainer>
  )
}

export default NavigationSettings