import styled from "styled-components";
import {DEFAULT_TRANSITION} from "@common/constants";
import { INavigationItemsStyle } from "./types";
import {ISThemeEditProperties} from "@common/types";

export const NavigationContainer = styled.div<ISThemeEditProperties>`
  transition: ${DEFAULT_TRANSITION};
  width: 100%;
  height: 48px;
  background: ${({ primaryColor }) => primaryColor ?? '#333'};
  color: ${({ textColor }) => textColor ?? 'white'};
  position: absolute;
  bottom: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: space-between;
  z-index: 999;
`;

export const NavigationBarContainer = styled.div`
  transition: ${DEFAULT_TRANSITION};
  background: rgba(0, 0, 0, .5);
  width: 48px;
  height: 48px;
  font-size: 18px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  position: relative;
  
  &:hover {
    background: #000000;
  }
`;

export const NavigationBarMenu = styled.div<ISThemeEditProperties>`
  position: absolute;
  left: 0;
  bottom: 48px;
  width: 350px;
  max-height: 400px;
  overflow-x: auto;
  background: ${({ primaryColor }) => primaryColor ?? '#333'};
  color: ${({ textColor }) => textColor ?? 'white'};
`;

export const NavigationElement = styled.div`
  transition: ${DEFAULT_TRANSITION};
  display: flex;
  align-items: center;
  
  &.isCenter {
    width: 100%;
  }
`;

export const NavigationItems = styled.div<INavigationItemsStyle>`
  transition: ${DEFAULT_TRANSITION};
  display: block;
  text-align: center;
  margin: 0 10px;
  width: ${({ isCenter, width }) => isCenter ? `100%` : `${width ? `${width}px` : 'auto'}` };
`;

export const NavigationItemContainer = styled.div<INavigationItemsStyle>`
  transition: ${DEFAULT_TRANSITION};
  display: inline-flex;
  width: ${({ tabSize }) => tabSize }px;
  height: ${({ tabSize }) => tabSize }px;
  border-radius: 3px;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  margin-right: 10px;
  border: 2px solid transparent;
  
  &:hover {
    background: rgba(255,255,255,.1);
  }
  
  img {
    width: ${({ iconSize }) => iconSize }px;
    height: ${({ iconSize }) => iconSize }px;
  }
  
  &.active {
    background: rgba(255, 255, 255, .2);
  }
  
  &.minimized {
    border-color: rgb(255, 255, 255, .2);
  }
  
  &.isOpened {
    background: rgba(255, 255, 255, .05);
  }
  
  &:last-child {
    margin-right: 0;
  }
`;

export const NavigationSettingsContainer = styled.div`
  transition: ${DEFAULT_TRANSITION};
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  height: 48px;
  width: 80px;
  font-size: 12px;
`;

export const NavigationSettingsClock = styled.div`
  text-align: right;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100%;
  width: 100%;

  &:hover {
    background: rgba(255, 255, 255, .05)
  }
`;

export const NavigationSettingsClockTime = styled.div`
  font-size: 16px;
  font-weight: bold;
  line-height: 20px;
`;

export const NavigationSettingsClockDate = styled.div`
  opacity: .7;
`;

export const NavigationSettingPanel = styled.div<ISThemeEditProperties>`
  position: absolute;
  right: 0;
  bottom: 48px;
  width: 350px;
  max-height: 450px;
  overflow-x: auto;
  font-size: 16px;
  background: ${({ primaryColor }) => primaryColor ?? '#333'};
  color: ${({ textColor }) => textColor ?? 'white'};
`;

export const NavigationSettingCard = styled.div`
  border-bottom: 1px solid rgba(0,0,0,.2);
  padding: 20px;
`;

export const NavigationSettingHeader = styled.div`
  font-size: 18px;
  margin-bottom: 20px;
`;

export const NavigationSettingContainer = styled.div`
  display: flex;
`;

export const NavigationSettingWallpaperItem = styled.div`
  cursor: pointer;
  width: 82px;
  height: 82px;
  border-radius: 10px;
  background: url(${({ property }) => property}) no-repeat center;
  background-size: cover;
  margin-right: 10px;
`;

export const NavigationSettingColorItem = styled.div`
  cursor: pointer;
  width: 32px;
  height: 32px;
  border-radius: 50%;
  background: ${({ color }) => color};
  margin-right: 10px;
`;

export const NavigationSettingButton = styled.div`
  transition: ${DEFAULT_TRANSITION};
  border: 3px solid rgba(255, 255, 255,.1);
  padding: 10px;
  border-radius: 5px;
  cursor: pointer;
  
  &:hover {
    background: rgba(255, 255, 255, .1);
    border-color: transparent;
  }
`;