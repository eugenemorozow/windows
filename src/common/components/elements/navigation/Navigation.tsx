import React, {FC, useEffect, useRef, useState} from 'react';
import {
  NavigationBarContainer,
  NavigationContainer,
  NavigationElement, NavigationItems
} from "@common/components/elements/navigation/Navigation.styles";
import NavigationItem from './NavigationItem';
import {INavigationItemProperties} from "@common/types";
import { IDefaultProps } from './types';
import {DEFAULT_NAVIGATION_HEIGHT, IS_FRONT} from "@common/constants";
import {useActions} from "@hooks/useActions";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBars} from "@fortawesome/free-solid-svg-icons";
import NavigationSettings from './NavigationSettings';
import {isReadable} from "stream";
import {useTypedSelector} from "@hooks/useTypedSelector";
import NavigationBar from "@common/components/elements/navigation/NavigationBar";

const Navigation: FC<IDefaultProps> = ({ applications, settings }) => {
  const actions = useActions()
  const navigationContainer = useRef<HTMLDivElement>(null)
  const navigationItems = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (IS_FRONT) {
      calculateItemPosition()
    }
  }, [])

  useEffect(() => {
    if (settings.isReload) {
      calculateItemPosition()
    }
  }, [settings])


  const calculateItemPosition = () => {
    const navigationHeight = (navigationContainer?.current?.clientHeight || DEFAULT_NAVIGATION_HEIGHT) / 2
    applications.filter(it => it.navigation).forEach(it => {
      const item = document.getElementById(`nav_item_${it.uid}`)
      if (item && navigationContainer.current) {
        actions.setApplication(it.uid, {
          navigationEdit: {
            top: navigationHeight,
            left: item.offsetLeft + (item.clientWidth / 2)
          }
        })
      }
    })
  }

  return (
    <NavigationContainer ref={navigationContainer}
                         primaryColor={settings.theme.primaryColor}
                         id="navigation">
      <NavigationElement>
        <NavigationBar applications={applications} settings={settings} />
      </NavigationElement>

      <NavigationElement className="isCenter">
        <NavigationItems ref={navigationItems}
                         isCenter={settings.navigation.application.isCenter}
                         width={settings.navigation.application.width}>
          {
            applications.map(app => <NavigationItem key={app.uid} application={app} settings={settings} />)
          }
        </NavigationItems>
      </NavigationElement>

      <NavigationElement>
        <NavigationSettings applications={applications} settings={settings} calculateItemPosition={calculateItemPosition} />
      </NavigationElement>
    </NavigationContainer>
  )
}

export default Navigation;