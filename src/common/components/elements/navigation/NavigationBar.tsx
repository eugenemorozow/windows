import React, {FC} from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBars} from "@fortawesome/free-solid-svg-icons";
import {NavigationBarContainer, NavigationBarMenu} from "@common/components/elements/navigation/Navigation.styles";
import {IDefaultProps} from "@common/components/elements/navigation/types";
import {useActions} from "@hooks/useActions";

const NavigationBar: FC<IDefaultProps> = ({ applications, settings }) => {
  const actions = useActions()

  const setMenu = () => {
    actions.setSetting({
      navigation: {
        menu: {
          isOpen: !settings.navigation.menu.isOpen
        }
      }
    })
  }

  return (
    <>
      {
        settings.navigation.menu.isOpen && (
          <NavigationBarMenu id="menuPanel"
                             primaryColor={settings.theme.primaryColor}
                             textColor={settings.theme.textColor}>
            <h2>Список приложений</h2>
            {
              applications.filter(it => it.inMenu).map(it => <p key={it.name}>{ it.name }</p>)
            }
          </NavigationBarMenu>
        )
      }
      <NavigationBarContainer onClick={setMenu}>
        <FontAwesomeIcon icon={faBars} />
      </NavigationBarContainer>
    </>
  )
}

export default NavigationBar