import {IApplicationProperties, ISettingProperties, ISThemeProperties} from "@common/types";

export interface IDefaultProps {
  applications: IApplicationProperties[]
  settings: ISettingProperties
}

export interface INavigationItemProps extends Omit<IDefaultProps, 'applications'> {
  application: IApplicationProperties
}

export interface ISettingPanelProps extends IDefaultProps {
  calculateItemPosition: () => void
}

export interface INavigationClock {
  time: string
  date: string
}

export interface INavigationItemsStyle {
  tabSize?: number
  iconSize?: number
  isCenter?: boolean
  width?: number | null
}
