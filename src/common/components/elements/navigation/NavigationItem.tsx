import React, {FC, useEffect} from "react";
import {NavigationItemContainer} from "@common/components/elements/navigation/Navigation.styles";
import {INavigationItemProps} from "@common/components/elements/navigation/types";
import {useActions} from "@hooks/useActions";

const NavigationItem: FC<INavigationItemProps> = ({ application, settings }) => {
  const actions = useActions()
  const classNames = application.window.isMinimized ? 'minimized'
                       : application.window.isCurrentActive ? 'active'
                         : application.window.isOpened ? 'isOpened' : ''

  const setWindowState = () => {
    const payload = {
      windowEdit: {
        isOpened: true,
        isCurrentActive: true,
        isMinimized: !application.window.isOpened ? false : !application.window.isMinimized,
      }
    }
    actions.setApplication(application.uid, payload)
  }

  return (
    <NavigationItemContainer tabSize={settings.navigation.application.tabSize}
                             iconSize={settings.navigation.application.iconSize}
                             id={`nav_item_${application.uid}`}
                             className={classNames}
                             onClick={setWindowState}>
      <img src={application.icon || ''} />
    </NavigationItemContainer>
  )
}

export default NavigationItem