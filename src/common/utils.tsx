// import {MouseEvent} from "react";
import {WINDOW_HEADER_DRAGGER} from "@common/constants";

export const setWindowPosition = (actions: any) => {
  let id: string | null = null
  let startX: number = 0, startY: number = 0
  let top: number = 0, left: number = 0
  let parent: HTMLElement | null = null
  let isResize = false

  function moveStart(e: MouseEvent) {
    e.preventDefault()
    // @ts-ignore
    const isString = typeof e.target.className === 'string'
    // @ts-ignore
    if (isString && e.target.className.includes(WINDOW_HEADER_DRAGGER) && e.target.id) {
      // @ts-ignore
      startX = e.offsetX
      // @ts-ignore
      startY = e.offsetY
      // @ts-ignore
      id = e.target.id
      parent = document.getElementById(`window_${id}`)
      if (id && parent) {
        if (window.innerWidth !== parent.clientWidth) {
          actions.setApplication(id, {
            windowEdit: {
              isMoving: true,
              isCurrentActive: true
            }
          })
        }
      }
    }
  }

  function moving(e: MouseEvent) {
    e.preventDefault()
    if (parent && id && window.innerWidth !== parent.clientWidth) {
      if (e.pageY < window.innerHeight - 48 && e.pageY >= startY) {
        top = e.pageY - startY
      }
      if (e.pageX < window.innerWidth && e.pageX >= 0) {
        left = e.pageX - startX
      }
      parent.style.top = `${top}px`
      parent.style.left = `${left}px`
    }
  }

  function moveEnd(e: MouseEvent) {
    e.preventDefault()
    if (id) {
      actions.setApplication(id, {
        windowEdit: {
          isMoving: false
        }
      })
    }
    parent = null
    id = null
    isResize = false
  }

  // @ts-ignore
  document.addEventListener('mousedown', (e) => moveStart(e))
  // @ts-ignore
  document.addEventListener('mousemove', (e) => moving(e))
  // @ts-ignore
  document.addEventListener('mouseup', (e) => moveEnd(e))
}