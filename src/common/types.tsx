import React from "react";

export interface IDefaultProps {
  children: React.ReactNode
}

export enum EAppType {
  IFRAME = 'IFRAME',
  TEXT = 'TEXT'
}

// Window

export interface IWindowProperties {
  width: number
  height: number
  top: number
  left: number
  prevTop: number
  prevLeft: number
  isOpened: boolean
  isMinimized: boolean
  isFullScreen: boolean
  isCurrentActive: boolean
  isMoving: boolean
}

export interface IWindowEditProperties {
  width?: number
  height?: number
  top?: number
  left?: number
  isOpened?: boolean
  isMinimized?: boolean
  isFullScreen?: boolean
  isCurrentActive?: boolean
  isMoving?: boolean
}

// Application

export interface INavigationItemProperties {
  top: number
  left: number
}
export interface INavigationItemEditProperties {
  top?: number
  left?: number
}

export interface IApplicationProperties {
  uid: string
  index: number
  name: string
  description?: string | null
  icon?: string | null
  type: EAppType
  content: any
  inMenu: boolean
  navigation?: INavigationItemProperties | null
  window: IWindowProperties
}

export interface IApplicationChangeParameters {
  icon?: string | null
  windowEdit?: IWindowEditProperties
  navigationEdit?: INavigationItemEditProperties
}

// Settings

export interface ISThemeProperties {
  primaryColor: string
  textColor: string
}

export interface ISThemeEditProperties {
  primaryColor?: string
  textColor?: string
}

export interface ISDesktopProperties {
  background: {
    url?: string | null
    color: string
  }
}

export interface ISDesktopEditProperties {
  background?: {
    url?: string | null
    color?: string
  }
}

export interface ISNMenuProperties {
  isOpen: boolean
}

export interface ISNMenuEditProperties {
  isOpen?: boolean
}

export interface ISNApplicationProperties {
  tabSize: number
  iconSize: number
  isCenter: boolean
  width: number
}

export interface ISNApplicationEditProperties {
  isCenter?: boolean
  width?: number
}

export interface ISNPanelProperties {
  isOpen: boolean
}

export interface ISNPanelEditProperties {
  isOpen?: boolean
}

export interface ISNavigationProperties {
  menu: ISNMenuProperties
  application: ISNApplicationProperties
  panel: ISNPanelProperties
}

export interface ISNavigationEditProperties {
  menu?: ISNMenuEditProperties
  application?: ISNApplicationEditProperties
  panel?: ISNPanelEditProperties
}

export interface ISettingProperties {
  isReload: boolean
  theme: ISThemeProperties
  desktop: ISDesktopProperties
  navigation: ISNavigationProperties
}

export interface ISettingEditProperties {
  theme?: ISThemeEditProperties
  desktop?: ISDesktopEditProperties
  navigation?: ISNavigationEditProperties
}

export interface ISettingPanelColor {
  name: string
  color: string
  isBlackFontColor: boolean
}

export interface ISettingPanelColor {
  name: string
  color: string
  isBlackFontColor: boolean
}

export interface ISettingWallpaper {
  url?: string
}