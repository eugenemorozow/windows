import {useActions} from "@hooks/useActions";
import {useTypedSelector} from "@hooks/useTypedSelector";
import {FC, useEffect, useState} from "react"

export const useEventListener = () => {
  const actions = useActions()
  const { settings } = useTypedSelector(state => state.app)

  function getPositionOfElements(e: MouseEvent) {
    // @ts-ignore
    if (e?.target?.id === 'root') {
      if (settings?.navigation.panel.isOpen) {
        actions.setSetting({
          navigation: {
            panel: {
              isOpen: false
            }
          }
        })
      }
      if (settings?.navigation.menu.isOpen) {
        actions.setSetting({
          navigation: {
            menu: {
              isOpen: false
            }
          }
        })
      }
    }

    // todo: запилить поиск по позиции открытого блока
    // if (typeof e.target.className === 'string' && e?.target?.id !== panel?.id && !e.target.className.includes('panelSafeArea')) {
    // }
    // if (typeof e.target.className === 'string' && e?.target?.id !== menu?.id && !e.target.className.includes('menuSafeArea')) {
    // }
  }

  useEffect(() => {
    document.addEventListener('click', (e: MouseEvent) => getPositionOfElements(e))
    return () => document.removeEventListener('click', (e) => getPositionOfElements(e))
  }, [])
}