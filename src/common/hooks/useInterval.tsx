import React, {useEffect, useRef, Dispatch} from 'react';

export const useInterval = (callback: Dispatch<any>, delay: number) => {
  const savedCallback = useRef<Dispatch<any>>();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function tick() {
      if (savedCallback.current) {
        // @ts-ignore
        savedCallback.current()
      }
    }
    if (delay !== null) {
      let id = setInterval(tick, delay)
      return () => clearInterval(id)
    }
  }, [delay])
}