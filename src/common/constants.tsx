export const DEFAULT_TRANSITION: string = '.23s ease-in-out'

export const DEFAULT_NAVIGATION_HEIGHT: number = 48

export const IS_FRONT: boolean = typeof window !== "undefined"

export const WINDOW_HEADER_DRAGGER: string = 'windowHeader'