import type { NextPage } from 'next'
import styles from '../styles/Home.module.css'
import Navigation from "@common/components/elements/navigation/Navigation";
import {useTypedSelector} from "@hooks/useTypedSelector";
import Window from "@common/components/elements/window/Window";
import {useEffect} from "react";
import {setWindowPosition} from "@common/utils";
import {IS_FRONT} from "@common/constants";
import {useActions} from "@hooks/useActions";
import {useEventListener} from "@hooks/useEventListener";
import {MainContainer} from "@common/styles";

const Home: NextPage = () => {
  const actions = useActions()
  const { applications, settings } = useTypedSelector(state => state.app)
  useEventListener()

  useEffect(() => {
    if (IS_FRONT) {
      setWindowPosition(actions)
    }
  }, [])

  return (
    <MainContainer id="root"
                   url={settings?.desktop.background.url || undefined}
                   color={settings?.desktop.background.color || undefined}>
      <div>
        {
          applications?.filter(app => app.window.isOpened).map(app =>
            <Window key={app.uid} application={app} settings={settings!!} />)
        }
      </div>
      <Navigation applications={applications!!} settings={settings!!} />
    </MainContainer>
  )
}

export default Home
