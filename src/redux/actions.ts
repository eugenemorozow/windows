import * as AppActions from './app/actions'

export const actionCreators = {
  ...AppActions,
}