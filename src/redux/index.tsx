import {applyMiddleware, createStore, Middleware, StoreEnhancer} from 'redux';
import {createWrapper, MakeStore} from 'next-redux-wrapper';
import {reducer, RootState} from '@redux/reducers';
import rootSaga from '@redux/sagas';
import createSagaMiddleware from '@redux-saga/core';


const bindMiddleware = (middleware: Middleware[]): StoreEnhancer => {
  if (process.env.NODE_ENV !== 'production') {
    const { composeWithDevTools } = require('redux-devtools-extension');
    return composeWithDevTools(applyMiddleware(...middleware));
  }
  return applyMiddleware(...middleware);
};


// @ts-ignore
export const makeStore: MakeStore<RootState> = () => {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(reducer, bindMiddleware([sagaMiddleware]));
  // @ts-ignore
  store.sagaTask = sagaMiddleware.run(rootSaga);
  return store;
};


// @ts-ignore
export const wrapper = createWrapper<RootState>(makeStore, { debug: true });