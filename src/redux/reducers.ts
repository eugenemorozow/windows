import {HYDRATE} from 'next-redux-wrapper';
import {combineReducers} from 'redux';
import {AppReducer} from './app/reducer';

const rootReducer = combineReducers({
  app: AppReducer,
})

export const reducer = (state: any, action: any) => {
  if (action.type === HYDRATE) {
    const nextState = {
      ...state,
      ...action.payload,
    };
    if (state.count) nextState.count = state.count;
    return nextState;
  } else {
    return rootReducer(state, action);
  }
};

export type RootState = ReturnType<typeof rootReducer>