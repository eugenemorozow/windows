import {
  IApplicationChangeParameters,
  IApplicationProperties,
  ISettingEditProperties,
  ISettingProperties
} from "@common/types";

export enum IApplicationActionType {
  SET_APPLICATION = 'SET_APPLICATION',
  SET_APPLICATIONS = 'SET_APPLICATIONS',
  SET_SETTINGS = 'SET_SETTINGS',
  SET_SETTING = 'SET_SETTING',
}

export interface IApplicationState {
  applications?: IApplicationProperties[]
  settings?: ISettingProperties
}

export interface IApplicationsSetAction {
  type: IApplicationActionType.SET_APPLICATIONS
  payload: IApplicationState
}

export interface IApplicationDetailSetAction {
  type: IApplicationActionType.SET_APPLICATION
  payload: IApplicationChangeParameters
  id: string
}

export interface ISettingDetailSetAction {
  type: IApplicationActionType.SET_SETTING
  payload: ISettingEditProperties
}

export interface ISettingsSetAction {
  type: IApplicationActionType.SET_SETTINGS
  payload: IApplicationState
}

export type IAppActions = IApplicationsSetAction | IApplicationDetailSetAction | ISettingDetailSetAction
  | ISettingsSetAction