import {call, put, select, takeEvery} from 'redux-saga/effects';
import {
  IApplicationActionType, IApplicationDetailSetAction, ISettingDetailSetAction,
} from '@redux/app/types';
import * as actions from './actions';
import {IApplicationProperties, ISettingEditProperties, ISettingProperties} from "@common/types";

const getApplications = (state: any) => state.app.applications
const getSettings = (state: any) => state.app.settings

function* setApplication({ payload, id }: IApplicationDetailSetAction) {
  const { windowEdit, navigationEdit } = payload
  const applications: IApplicationProperties[] = yield select(getApplications)
  let application = applications.filter(app => app.uid === id)[0]
  const element: HTMLElement | null = document.getElementById(`window_${id}`)

  if (windowEdit?.isOpened !== undefined) {
    application.window.isOpened = windowEdit.isOpened
    if (!windowEdit.isOpened && element) {
      application.window.top = element.offsetTop
      application.window.left = element.offsetLeft
    }
  }

  if (windowEdit?.isMinimized !== undefined) {
    if (element) {
      if (windowEdit.isMinimized) {
        application.window.prevTop = element.offsetTop
        application.window.prevLeft = element.offsetLeft
      }
      element.style.top = `${windowEdit.isMinimized ? window.innerHeight - (application.navigation?.top || 0) : application.window.prevTop}px`
      element.style.left = `${windowEdit.isMinimized ? (application.navigation?.left || 0) : application.window.prevLeft}px`
    }
    application.window.isMinimized = windowEdit.isMinimized
  }

  if (windowEdit?.isFullScreen !== undefined) {
    if (element) {
      if (windowEdit.isFullScreen) {
        application.window.prevTop = element.offsetTop
        application.window.prevLeft = element.offsetLeft
      }
      element.style.top = `${windowEdit.isFullScreen ? 0 : application.window.prevTop}px`
      element.style.left = `${windowEdit.isFullScreen ? 0 : application.window.prevLeft}px`
    }
    application.window.isFullScreen = windowEdit.isFullScreen
  }

  if (windowEdit?.isCurrentActive !== undefined) {
    applications.map(it => it.window.isCurrentActive = false)
    if (windowEdit?.isCurrentActive) {
      application.window.isCurrentActive = true
    }
  }

  if (windowEdit?.isMoving !== undefined) {
    application.window.isMoving = windowEdit.isMoving
  }

  if (windowEdit?.top && windowEdit?.left) {
    application.window.top = windowEdit.top
    application.window.left = windowEdit.left
  }

  if (navigationEdit?.top && navigationEdit?.left) {
    if (application.navigation) {
      application.navigation.top = navigationEdit.top
      application.navigation.left = navigationEdit.left
    }
  }

  yield put(actions.setApplications({
    applications: [...applications],
  }))
}

function* setSettings({ payload }: ISettingDetailSetAction) {
  const { theme, navigation, desktop } = payload
  const settings: ISettingProperties = yield select(getSettings)

  if (theme) {
    if (theme?.primaryColor !== undefined) {
      settings.theme.primaryColor = theme.primaryColor
    }

    if (theme?.textColor !== undefined) {
      settings.theme.textColor = theme.textColor
    }
  }

  if (desktop) {
    if (desktop?.background !== undefined) {
      if (desktop.background?.url !== undefined) {
        settings.desktop.background.url = desktop.background.url
      }

      if (desktop.background?.color !== undefined) {
        settings.desktop.background.color = desktop.background.color
      }
    }
  }

  if (navigation) {
    if (navigation?.menu && navigation?.menu?.isOpen !== undefined) {
      settings.navigation.menu.isOpen = navigation.menu.isOpen
    }

    if (navigation?.panel && navigation?.panel?.isOpen !== undefined) {
      settings.navigation.panel.isOpen = navigation.panel.isOpen
    }

    if (navigation?.application !== undefined) {
      if (navigation.application?.isCenter !== undefined) {
        settings.navigation.application.isCenter = navigation.application.isCenter
      }
      if (navigation.application?.width !== undefined) {
        settings.navigation.application.width = navigation.application.width
      }
    }
  }

  yield put(actions.setSettings({
    settings: {...settings, isReload: false}
  }))
}


export default [
  takeEvery(IApplicationActionType.SET_APPLICATION, setApplication),
  takeEvery(IApplicationActionType.SET_SETTING, setSettings)
]