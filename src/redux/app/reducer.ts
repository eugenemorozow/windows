import {IAppActions, IApplicationActionType, IApplicationState} from '@redux/app/types';
import {APPLICATIONS, SETTINGS} from "@common/mocks";
import update from "immutability-helper";

const initialState: IApplicationState = {
  applications: APPLICATIONS,
  settings: SETTINGS,
}

export const AppReducer = (state = initialState, action: IAppActions): IApplicationState => {
  switch (action.type) {
    case IApplicationActionType.SET_APPLICATIONS:
      return update(state, {$merge: action.payload})
    case IApplicationActionType.SET_APPLICATION:
      return state

    case IApplicationActionType.SET_SETTINGS:
      return update(state, {$merge: action.payload})
    case IApplicationActionType.SET_SETTING:
      return state

    default:
      return state
  }
}