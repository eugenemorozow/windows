import {
  IApplicationActionType,
  IApplicationDetailSetAction, IApplicationsSetAction,
  IApplicationState, ISettingDetailSetAction
} from '@redux/app/types';
import {IApplicationChangeParameters, ISettingEditProperties} from "@common/types";

export const setApplication = (id: string, payload: IApplicationChangeParameters): IApplicationDetailSetAction => ({
  type: IApplicationActionType.SET_APPLICATION,
  id: id,
  payload: payload
})

export const setApplications = (payload: IApplicationState): IApplicationsSetAction => ({
  type: IApplicationActionType.SET_APPLICATIONS,
  payload: payload
})

export const setSetting = (payload: ISettingEditProperties): ISettingDetailSetAction => ({
  type: IApplicationActionType.SET_SETTING,
  payload: payload
})

export const setSettings = (payload: IApplicationState): IApplicationsSetAction => ({
  type: IApplicationActionType.SET_APPLICATIONS,
  payload: payload
})